import java.util.Scanner;

public class NombresCapicúa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	
		  Scanner src = new Scanner(System.in);

	        while (true) {
	 
	            String input = src.nextLine();

	            if (input.equals("0")) {
	                break;
	            }

	            int length = input.length();
	            boolean Simetric = true;

	            for (int i = 0; i < length / 2; i++) {
	                if (input.charAt(i) != input.charAt(length - 1 - i)) {
	                	Simetric = false;
	                    break;
	                }
	            }

	            if (Simetric) {
	                System.out.println("Si");
	            } else {
	                System.out.println("No");
	            }
	        }
	    }
	}
