import java.util.Scanner;
import java.util.Stack;

public class ParentesisBalanceados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		        Scanner src = new Scanner(System.in);

		        while (true) {
		            String input = src.nextLine();
		            if (input.equals("")) {
		                return;
		            }

		            int len = input.length();
		            boolean res = true;
		            Stack<Character> symbols = new Stack<>();
		            
		            char character1 = src.nextLine().charAt(len);
	                char character2 = src.nextLine().charAt(len);
	                
		            for (int i = 0; i < len && res; i++) {
		                char Char = input.charAt(i);
		               

		                if (Char == '(') {
		                    symbols.push(Char);
		                } else if (Char == ')' ) {
		                    if (!symbols.isEmpty() && (character1 == '(' && character2 == ')')) {
		                        symbols.pop();
		                    } else {
		                        res = false;
		                    }
		                }
		            }

		            if (res && symbols.isEmpty()) {
		                System.out.println("SI");
		            } else {
		                System.out.println("NO");
		            }
		        }
		    }

		
		}
