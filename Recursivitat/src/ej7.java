import java.util.Scanner;

public class ej7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
        int base = 2;
        int exponente = 3;

        double resultado = exp(base, exponente);
        System.out.println(base + " elevado a la " + exponente + " es igual a: " + resultado);
    }

    // Función recursiva para calcular la potencia (exponenciación)
    public static double exp(int n, int x) {
        double res;

        if (x == 0) {
            res = 1;
        } else if (x > 0) {
            res = n * exp(n, x - 1);
        } else { // Si x es negativo
            res = 1.0 / (n * exp(n, -x - 1));
        } 

        return res;


	}

}
