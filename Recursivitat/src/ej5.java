

import java.util.Scanner;

public class ej5 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int n = src.nextInt();
		int res = sumaDigits(n);
		System.out.println(res);

		
	}
	
	public static int sumaDigits(int n) {
		int res = 0;
		
		if (n%10==0) res = 0;
		
		else res = n%10 + sumaDigits(n/10);
				return res;
	}
	
}
