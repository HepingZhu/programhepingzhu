

import java.util.Scanner;

public class ej2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int n= src.nextInt();
		
		double res = numeroE(n);
		
		System.out.println("Resultat: "+ res);

	}

		public static double numeroE(int n ) {
			double res;
			if (n==0) res = 1;
			else res =numeroE(n-1)+1.0/factorial(n);
			
			
			return res; 
		
	}

		  private static long factorial(int n) {
		        if (n == 0 || n == 1) {
		            return 1;
		        } else {
		            return n * factorial(n - 1);
		        }
		    }
		}