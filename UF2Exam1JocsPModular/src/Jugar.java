import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Jugar {
	static Scanner src = new Scanner(System.in);
	    private static char[][] tablero = new char[8][8]; 
	    private static final int MAX_MOVIMIENTOS = 64;
	    private static Map <String, Integer> victoriasPorJugador = new HashMap<>();
	  
	    public static void main(String[] args) {
	    	// TODO Auto-generated method stub
	    	
	    	
	        String jugador1 = ObtenirNom(1);
	        String jugador2 = ObtenirNom(2);

	        boolean resultado = jugarPartida(jugador1, jugador2);

	        if (resultado) {
	        	registrarVictoria(jugador1, jugador2);
	        } else {
	            System.out.println("La partida ha terminado en empate.");
	        }
	    
	    }
	    
	 
		public static String determinarGanador(String jugador1, String jugador2) {
	    	// TODO Auto-generated method stub
	    	
	        int fichasJugador1 = contarFichas('X');
	        int fichasJugador2 = contarFichas('O');

	        if (fichasJugador1 > fichasJugador2) {
	            return jugador1;
	        } else if (fichasJugador1 < fichasJugador2) {
	            return jugador2;
	        } else {
	            return "Ambos jugadores";
	        }
	    }
	   
	    private static int contarFichas(char ficha) {
	    	// TODO Auto-generated method stub
	        int contador = 0;
	        for (int i = 0; i < 8; i++) {
	            for (int j = 0; j < 8; j++) {
	                if (tablero[i][j] == ficha) {
	                    contador++;
	                }
	            }
	        }
	        return contador;
	    }
	    
		public static String ObtenirNom(int numeroJugador) {
			// TODO Auto-generated method stub
	        System.out.print("Introduce el nombre del Jugador " + numeroJugador + ": ");
	        return src.nextLine();
	    }
	

	    public static boolean jugarPartida(String jugador1, String jugador2) {
	    	// TODO Auto-generated method stub
	        inicializarTablero();

	        boolean turnoJugador1 = true;
	        int movimientos = 0;
	        
	       
	        while (movimientos < MAX_MOVIMIENTOS) {
	            mostrarTablero();

	            int columna = obtenerColumna(turnoJugador1 ? jugador1 : jugador2);

	            if (!insertarFicha(columna, turnoJugador1 ? 'X' : 'O')) {
	                System.out.println("Columna llena. Elige otra columna.");
	                continue;
	            }

	            if (comprobarVictoria(columna, turnoJugador1 ? 'X' : 'O')) {
	                mostrarTablero();
	                System.out.println("¡" + (turnoJugador1 ? jugador1 : jugador2) + " ha ganado!");
	                return true;
	            }
  
	            movimientos++;
	            turnoJugador1 = !turnoJugador1; // Cambiar el turno al otro jugador
	        }

	        mostrarTablero();
	        System.out.println("La partida ha terminado en empate.");
	        return false;
	    }

	    private static void inicializarTablero() {
	    	// TODO Auto-generated method stub
	    	
	        for (int i = 0; i < 8; i++) {
	            for (int j = 0; j < 8; j++) {
	                tablero[i][j] = ' ';
	            }
	        }
	    }

	    private static void mostrarTablero() {
	    	// TODO Auto-generated method stub
	        System.out.println(" 0 1 2 3 4 5 6 7");
	        for (int i = 0; i < 8; i++) {
	            System.out.print("|");
	            for (int j = 0; j < 8; j++) {
	                System.out.print(tablero[i][j] + "|");
	            }
	            System.out.println();
	        }
	    }

	    private static int obtenerColumna(String jugador) {
	    	// TODO Auto-generated method stub
	    	
	        int columna;
	        do {
	            System.out.print(jugador + ", elige una columna (0-7): ");
	            columna = src.nextInt();
	        } while (columna < 0 || columna > 7);
	        return columna;
	    }

	    private static boolean insertarFicha(int columna, char ficha) {
	    	// TODO Auto-generated method stub
	        for (int i = 7; i >= 0; i--) {
	            if (tablero[i][columna] == ' ') {
	                tablero[i][columna] = ficha;
	                return true;
	            }
	        }
	        return false; // Columna llena
	    }

	    private static boolean comprobarVictoria(int columna, char ficha) {
	    	// TODO Auto-generated method stub
	        
	        if (verificarColumna(columna, ficha)) {
	            return true;
	        }

	        
	        if (verificarFila(ficha)) {
	            return true;
	        }

	        if (verificarDiagonales(ficha)) {
	            return true;
	        }

	        return false;
	    }

	    private static boolean verificarColumna(int columna, char ficha) {
	    	// TODO Auto-generated method stub
	        for (int i = 0; i <= 4; i++) {
	            if (tablero[i][columna] == ficha &&
	                tablero[i + 1][columna] == ficha &&
	                tablero[i + 2][columna] == ficha &&
	                tablero[i + 3][columna] == ficha) {
	                return true;
	            }
	        }
	        return false;
	    }

	    private static boolean verificarFila(char ficha) {
	    	// TODO Auto-generated method stub
	        for (int i = 0; i < 8; i++) {
	            for (int j = 0; j <= 3; j++) {
	                if (tablero[i][j] == ficha &&
	                    tablero[i][j + 1] == ficha &&
	                    tablero[i][j + 2] == ficha &&
	                    tablero[i][j + 3] == ficha) {
	                    return true;
	                }
	            }
	        }
	        return false;
	    }

	    private static boolean verificarDiagonales(char ficha) {
	    	// TODO Auto-generated method stub
	        for (int i = 0; i <= 4; i++) {
	            for (int j = 0; j <= 4; j++) {
	                if (tablero[i][j] == ficha &&
	                    tablero[i + 1][j + 1] == ficha &&
	                    tablero[i + 2][j + 2] == ficha &&
	                    tablero[i + 3][j + 3] == ficha) {
	                    return true;
	                }
	            }
	        }

	       
	        for (int i = 0; i <= 4; i++) {
	            for (int j = 3; j < 8; j++) {
	                if (tablero[i][j] == ficha &&
	                    tablero[i + 1][j - 1] == ficha &&
	                    tablero[i + 2][j - 2] == ficha &&
	                    tablero[i + 3][j - 3] == ficha) {
	                    return true;
	                }
	            }
	        }

	        return false;
	    }
	

	public static void jugadorExpert(String jugador, String jugador2) {
		// TODO Auto-generated method stub
		if (victoriasPorJugador.isEmpty()) {
			System.out.println("No hi ha resultats");
		}else {
			String jugadorExpert = determinarJugadorExpert();
			System.out.print("El jugador expert és: "+ jugadorExpert);
		}
		
		
		
	}
	
	   private static void registrarVictoria(String jugador1, String jugador2) {
				// TODO Auto-generated method stub
				victoriasPorJugador.put(jugador1, victoriasPorJugador.getOrDefault(jugador1, 0)+1 );
			}

	   
	
	private static String determinarJugadorExpert() {
		// TODO Auto-generated method stub
		String jugadorExpert = null;
		int MaxVictorias = 0;
		  for (Map.Entry<String, Integer> entry : victoriasPorJugador.entrySet()) {
			  if (entry.getValue() > MaxVictorias) {
				  MaxVictorias = entry.getValue();
				  jugadorExpert = entry.getKey();
			  }
		  }
		
		  return jugadorExpert;
	}


}
