import java.util.Scanner;

public class CompteAmbElVeri {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner src = new Scanner(System.in);
	    

		  int casos = src.nextInt();
	        src.nextLine(); // Consumir la nueva línea

	        for (int i = 0; i < casos; i++) {
	            int hp = src.nextInt();
	            int rammus = src.nextInt();
	            int twitch = src.nextInt();
	            src.nextLine(); // Consumir la nueva línea

	            int rondas = 0;

	            while (hp > 0) {
	                hp -= rammus; // Rammus ataca
	                rondas++;

	                if (hp <= 0) {
	                    System.out.println("RAMMUS " + rondas);
	                    break;
	                }

	                hp -= twitch; // Twitch ataca
	                rondas++;

	                if (hp <= 0) {
	                    System.out.println("TWITCH " + rondas);
	                    break;
	                }
	            }
	        }

	        src.close();
	    }
	}