import java.util.Scanner;

public class Bucles1ValorMesGran {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
	       Scanner src = new Scanner(System.in);
	        
	        int max = Integer.MIN_VALUE;  // Inicializar max con un valor muy pequeño.
	        int min = Integer.MAX_VALUE;  // Inicializar min con un valor muy grande.

	        int num = src.nextInt();
	        while (num != 0) {
	            if (num < min) {
	                min = num;
	            }
	            if (num > max) {
	                max = num;
	            }
	            num = src.nextInt();
	        }

	        System.out.println(max + " " + min);
	        
	      
	    }
	}