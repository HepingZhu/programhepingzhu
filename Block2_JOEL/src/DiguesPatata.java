import java.util.Scanner;

public class DiguesPatata {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner src = new Scanner(System.in);
		int casos; // Indica el nombre de casos a tractar
		String entrada; //Guarda cada linia que llegim
		
		casos = src.nextInt();
		src.nextLine(); //Neteja el buffer d'entrada
		
		while (casos > 0) {
			//comença el tractament d'un cas de prova
			entrada = src.nextLine();
			System.out.println(entrada);
			//fi de la prova
			casos = casos -1; //= casos--;
		}
		
	}

}