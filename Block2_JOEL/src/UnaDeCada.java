import java.util.Scanner;

public class UnaDeCada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner src = new Scanner(System.in);
		int casos;
		String frase;
		String solucio;
		int i;
		boolean majuscula;
		int cont;

		casos = src.nextInt();
		src.nextLine();
		majuscula = false;
		
		while (casos > 0) {
			//Tractar un cas
			frase = src.nextLine();
			solucio = "";
			i = 0;
			
			
			while (i < frase.length()) {
				if ((frase.charAt(i) >= 'A' && frase.charAt(i) <= 'Z') ||
						(frase.charAt(i) >= 'a' && frase.charAt(i) <= 'z')) {
							if (majuscula == true) {
								solucio = solucio + Character.toUpperCase(frase.charAt(i));		
							}
							else {
								solucio = solucio + Character.toLowerCase(frase.charAt(i));
							}
						}
				else {  //El caràcter de la posició i no és lletra
					solucio = solucio + frase.charAt(i);
				}
				majuscula = !majuscula;
				i++;
			}
			System.out.println(solucio);
			
			
			
			//fi de tractar un cas
			casos--;
		}
		
		
	}


}
