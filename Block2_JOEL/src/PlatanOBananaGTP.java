import java.util.Scanner;

public class PlatanOBananaGTP {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		  Scanner scanner = new Scanner(System.in);
	        
	        int contadorP = 0;
	        int contadorB = 0;
	        
	        while (true) {
	            String linea = scanner.nextLine();
	            if (linea.equals("P")) {
	                contadorP++;
	            } else if (linea.equals("B")) {
	                contadorB++;
	            } else if (linea.equals("0")) {
	                break;
	            }
	        }
	        
	        scanner.close();
	        
	        if (contadorP > contadorB) {
	            System.out.println("M'agraden els platans");
	        } else if (contadorB > contadorP) {
	            System.out.println("M'agraden les bananes");
	        } else {
	            System.out.println("No puc distingir entre un platan i una banana");
	        }
	    }
	}