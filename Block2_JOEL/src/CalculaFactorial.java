import java.util.Scanner;

public class CalculaFactorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner(System.in);
		int casos;
		int num;
		long fact; // guarda el resultat de calcular el factorial de num
		
		casos= src.nextInt();
	
		while (casos>0){
			num = src.nextInt();
			fact=1;
			
			while (num>0) {
				fact = fact*num;
				num--;
				
				
			}
			System.out.println(fact);
			casos--;
		}

	}

}