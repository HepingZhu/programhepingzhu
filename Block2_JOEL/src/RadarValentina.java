import java.util.Scanner;

public class RadarValentina {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);

		int casos = src.nextInt();
		int medicio;
		int cont;
		boolean m, b, h;

		while (casos > 0) {

			m = false;
			b = false;
			h = false;

			cont = 5;
			while (cont > 0) {

				medicio = src.nextInt();
				if (medicio >= 10000) {
					m = true;
				} else {
					if (medicio >= 1000) {
						b = true;
					} else {
						h = true;
					}
				}
				cont--;
			}
			if (m == true)
				System.out.println("M");
			else if (b == true)
				System.out.println("B");
			else
				System.out.println("H");

			casos--;

		}

	}

}



