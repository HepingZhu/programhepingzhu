import java.util.Scanner;

public class ComptarParaules {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		
		String frase;
		
		int cont = 0, pos, totalCar, paraules;
		
		while (casos > 0) {
			//tractar una frase
			
			frase = src.nextLine();
			
			totalCar = frase.length();
			pos = 1;
			paraules = 0;
			while (pos<totalCar) {
				if (frase.charAt(pos) == ' ' && frase.charAt(pos-1) != ' ' ) {
					paraules++;		
				}
				pos++;
			}
			if (frase.charAt(pos-1) == ' ')
			    System.out.println(paraules);
			else
				System.out.println(paraules + 1);
			
			casos--;
			
		}
		src.close();
	}
}
