import java.util.Scanner;

public class EspiralGalactica {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner(System.in);

        int n = src.nextInt();
        while (n != 0) {
            int[][] M = new int[n][n];

            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    M[i][j] = src.nextInt();

            int i = n / 2, j = n / 2;
            int[][] dirs = { {-1, 0}, {0, 1}, {1, 0}, {0, -1}};
            int dirAct = 0;
            int numMov = 1;
            int numRestantes = 2;
            long suma = 0;

            while (i >= 0 && i < n && j >= 0 && j < n) {
                suma += M[i][j];
                numRestantes--;

                if (numRestantes == 0) {
                    numMov++;
                    numRestantes = numMov;
                    dirAct = (dirAct + 1) % 4;
                }

                i += dirs[dirAct][0];
                j += dirs[dirAct][1];
            }

            System.out.println(suma);

            n = src.nextInt();
        }

        src.close();
    }
}
