import java.util.Scanner;

public class Calculadora {

	
static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int v1 = 0, v2 = 0;  //valors per operar
		double res = 0;     //guarda el resultat de l'operació
		int op;
			
		
		do {
			op = mostrarMenu();
			
			switch (op) {
			 	case 1: v1 = obtenirNum();
						v2 = obtenirNum();
						break;
			 	case 2: res = suma(v1, v2);
			 	        break;
			 	case 3: res = resta(v1, v2);
	 	        		break;
			 	case 4: res = multiplicacio(v1, v2);
	 	        		break;
			 	case 5: res = divisio(v1, v2);
	 	        		break;
			 	case 6: mostrarResultat(res);
	     				break;
	     		case 0: System.out.println("Has finalitzat");
	     				break;
	     		default: System.out.println("Error, opció incorrecta");
			}
		} while (op != 0);
	}

	//Mostra per pantalla el resultat de l'operació calculada
	//res conté el resultat
		private static void mostrarResultat(double res) {
			// r és el paràmetre que conté el valor de la variable res del main
			System.out.println("El resultat és: " + res);
		}
	
		private static double divisio(double v1, double v2) {
			// TODO Auto-generated method stub
			double r= v1/v2;
			return r;
		}
	
		private static int multiplicacio(int v1, int v2) {
			// TODO Auto-generated method stub
			
			int r = v1*v2;
			return r;
		}
	
		private static int resta(int v1, int v2) {
			// TODO Auto-generated method stub
			
			int r = v1-v2;
			return r;
		}
	
		private static int suma(int op1, int op2) {
			// TODO Auto-generated method stub
			int r = op1 + op2;
			return r;
		}
	
		private static int obtenirNum() {
			// TODO Auto-generated method stub
			
			int v = 0;
			boolean correcte = false;
			
			do {
				System.out.print("Introdueix un valor enter: ");
			
				try {
					v = src.nextInt();
					correcte = true;
				}
				catch (Exception e) {
					System.out.println("Error, cal introduir un número enter");
					
					src.nextLine();
				}
			} while (!correcte);
			
			return v;
		}
	
		//Mostra per pantalla el menú d'opions
		private static int mostrarMenu() {
			// TODO Auto-generated method stub
			int op = 0;
			
			System.out.println("\nCalculadora:\n");
			System.out.println("1.- Obtenir els valors");
			System.out.println("2.- Sumar");
			System.out.println("3.- Restar");
			System.out.println("4.- Multiplicar");
			System.out.println("5.- Dividir");
			System.out.println("6.- Mostrar resultat");
			System.out.println("0.- Sortir");
			//System.out.print("\n\nTria una opció: ");
			
			op = obtenirNum();
			
			
			return op;
		}
	
	
	
	}
