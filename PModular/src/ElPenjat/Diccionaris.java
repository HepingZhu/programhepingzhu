package ElPenjat;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Scanner;

/* Apunts de suport
 * https://codegym.cc/es/groups/posts/es.653.como-crear-un-diccionario-en-java
 * https://pablomonteserin.com/curso/java/java-map/
 */


public class Diccionaris {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		//Dictionary phoneBook = new Hashtable(); //Creació del diccionai buit
		Hashtable<String, String> phoneBook = new Hashtable<String, String> ();
		  
		// put() method
        phoneBook.put("Johnny Walker", "2178");
        phoneBook.put("Andrew Arnold", "1298");
        phoneBook.put("Ivy Briggs", "1877");
        phoneBook.put("Ricky McBright", "2001");

        //print out Hashtable out
        System.out.println(phoneBook);

        //let's get the value using the key
        System.out.println(phoneBook.get("Ivy Briggs"));
        //there's no such key in out Hashtable
        System.out.println(phoneBook.get("Sol Frank"));

        //Is there a record with the "Johnny Walker" key?
        System.out.println((phoneBook).containsKey("Johnny Walker"));
        //all keys of the Hashtable
        System.out.println((phoneBook).keySet());
        //values from Hashtable
        System.out.println((phoneBook).values());

        //the quantity of records
        System.out.println(phoneBook.size());
        //removing one record
        phoneBook.remove("Andrew Arnold");
        System.out.println(phoneBook);
        
        //Recorrer un Map usando el bucle for
        //Para cada elemento key del conjunto map.keySet()
        for (String key : phoneBook.keySet()){
        	System.out.println(key + "=> " + phoneBook.get(key));
        }
        //Buidar el diccionari
        phoneBook.clear();
    }
	

}
