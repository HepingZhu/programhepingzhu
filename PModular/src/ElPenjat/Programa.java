package ElPenjat;

import java.util.Scanner;

/**
 * Classe que llança l'aplicació del penjat. És un distribuidor de tasques
 * mitjançant un menú d'opcions
 */

public class Programa {

	/**
	 *  1.- Mostrar Ajuda
		2.- Definir Jugador
		3.- Jugar Partida
		4.- Veure Jugador
		5.- Estadística de jugadors
		0.- Sortir
	 */
	
	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int op = 0;  //guarda l'opció escollida per l'usuari
		String jugador = null;  //guarda el nom del jugador actual
		boolean resultat;
		boolean definit = false;
		
		do {
			op = mostrarMenú();
			
			switch (op) {
				case 1: AjudaHayQueVer.mostrarAjuda();
						break;
				case 2: jugador = Jugador.definirJugador();
						definit = true;
						break;
				case 3: if (definit == true) {
							resultat = Joc.jugarPartida(jugador);
							Jugador.actualitzaResultat(jugador, resultat);
							definit = false;  //obliguem a definir el jugador si volem jugar altra partida
						}
						else System.out.println("No pots jugar fins que no estigui definit el jugador");
						break;
				case 4: Jugador.mostraResultats(jugador);
						break;
				case 5: Jugador.mostrarEstadistica();
						break;
				case 0: System.out.println("Fi de joc");
						break;
				default: System.out.println("Opció incorrecta");
			}
			
		} while (op != 0);
		
	}

	private static int mostrarMenú() {
		
		//Scanner src = new Scanner(System.in);
		// TODO Auto-generated method stub
		int op = 0; 
		boolean correcte = true;
		
		System.out.println("Joc del Penjat");
		System.out.println("1. Mostrar Ajuda");
		System.out.println("2. Definir jugador");
		System.out.println("3. Jugar partida");
		System.out.println("4. Resultats jugador");
		System.out.println("5. Estadístiques");
		System.out.println("0. Sortir");
		
		do {
			System.out.print("Opció: ");
			try {
				op = src.nextInt();
			}
			catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte = false;
			}
			
		} while (correcte == false);
		
		//src.close();
		
		return op;
	}

}
