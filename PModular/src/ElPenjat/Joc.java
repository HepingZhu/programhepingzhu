package ElPenjat;


import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Clase que implementa el joc del penjat
 * El mètode de jugar rebrà el jugador actual
 */

public class Joc {

	static Scanner src = new Scanner(System.in);
	
	public static boolean jugarPartida(String jugador) {
		// TODO Auto-generated method stub
		
		//Variables necessaries
		boolean guanya = false;		//Guarda el resultat de la partida
		ArrayList<String> paraules; //Guarda la llista de paraules que es poden jugar
		String paraula;				//Guarda la paraula que cal esbrinar
		char [] palUsuari;			//Guarda la paraula que està esbrinant el jugador
		int intents = 0;			//guarda els intents gastats pel jugador
		ArrayList<Character> lletres;  //guarda les lletres que s'han fet servir
		char lletra;				//guarda la lletra sel·leccionada pel jugador
		final int MAX = 12;			//Nombre màxim d'intents
		
		//Inicialitzar la partida
		paraules = carregaParaules();
		lletres = carregaLletres();
		paraula = seleccionarParaula(paraules);
		palUsuari = crearPalUsuari(paraula.length());
		
		//Comença la parida
		do {
			mostrarEstatPartida(intents, lletres, palUsuari);
			lletra = demanarLletra(lletres);
			intents++;
			actualitzarPalUsuari(lletra, paraula, palUsuari);
			guanya = comprovarFinal(palUsuari, paraula);
			
		} while (intents < MAX && guanya == false);
		
		// ME QUEDAO AQUI BRO
		mostrarEstatPartida(intents, lletres, palUsuari);
		if (guanya == true) System.out.println("Felicitats!!! Has guanyat");

		
		return guanya;
	}

	/**
	 * Comprova si palUsuari és igual que paraula
	 * @param palUsuari
	 * @param paraula
	 * @return true si les dues paraules són iguals, false en cas contrari
	 */
	private static boolean comprovarFinal(char[] palUsuari, String paraula) {
		// TODO Auto-generated method stub
		boolean res = true;
		
		int i = 0;
		
		while (i < paraula.length() && res == true) {
			if (paraula.charAt(i) != palUsuari[i])
				res = false;
			else i++;
		}
		
		return res;
	}
	
	/**
	 * Actualitza el vector palUsuari amb la lletra que rep per paràmetre segons indica la paraula de l'String
	 * @param lletra: caràcter sel.leciinat per l'usuari
	 * @param paraula: String amb la paraula a esbrinar
	 * @param palUsuari: Vector amb l'estat actul de la paraula del jugador
	 */
	private static void actualitzarPalUsuari(char lletra, String paraula, char[] palUsuari) {
		// TODO Auto-generated method stub
		for (int i = 0; i < paraula.length(); i++) {
			
			if (paraula.charAt(i) == lletra) {
				palUsuari[i] = lletra;
			}
		}
	}

	/**
	 * Rep un ArrayList amb les lletres que el jugador encara no ha fet servir i ho actualitza demanant al jugador la lletra que vol fer servir
	 * @param lletres
	 * @return lletra sel.leccionada pel jugador
	 */
	private static char demanarLletra(ArrayList<Character> lletres) {
		// TODO Auto-generated method stub
		
		char car;
		
		do {
			System.out.print("lletra: ");
			car = src.nextLine().toUpperCase().charAt(0);
			if (!lletres.contains(car)) {
				System.out.println("Lletra incorrecta");
			}
		
		} while (!lletres.contains(car));
		
		lletres.remove((Character)car);
		
		return car;
	}

	/**
	 * Mostra per pantalla l'estat de la partida
	 * @param intents: indica el nombre d'intnents gastats pel jugador
	 * @param lletres: ArrayList amb les lletres no gastades
	 * @param palUsuari
	 */
	private static void mostrarEstatPartida(int intents, ArrayList<Character> lletres, char[] palUsuari) {
		// TODO Auto-generated method stub
		System.out.println("........... Estat de la partida ...........");
		System.out.println("Intents: " + intents);
		System.out.println();
		System.out.println("Lletres disponibles:");
		for (int i = 0; i < lletres.size(); i++) {
			System.out.print(lletres.get(i) + " - ");
		}
		System.out.print("\n\nParaula: ");
		for (int i = 0; i < palUsuari.length; i++)
			System.out.print(palUsuari[i] + " ");
		System.out.println("\n........... FI ...................");
	}
	
	/**
	 * Crea un vector de caràcters de longitud length, passat per paràmetre, tot ell inicialitzat amb el caràcter '_'
	 * @param length: Indica la longitud de la paraula a esbrinar
	 * @return Vector amb la paraula de l'usuari inicialitzada tota ella amb '_'
	 */
	private static char[] crearPalUsuari(int length) {
		// TODO Auto-generated method stub
		char [] vec = new char[length];
		
		for (int i = 0; i < length; i++)
			vec[i] = '_';
		
		return vec;
	}

	/**
	 * Sel.lecciona de la llisa de paraules una paraula de forma aleatòria i la retorna
	 * @param paraules
	 * @return String amb la paraula sel.leccionada de la llista
	 */
	private static String seleccionarParaula(ArrayList<String> paraules) {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		
		return paraules.get(rnd.nextInt(paraules.size()));
	}

	/**
	 * Inicialitza un ArrayList de caràcters amb les 26 lletres de l'abecedari anglès
	 * @return la llista amb les 26 lletres en majúscules
	 */
	private static ArrayList<Character> carregaLletres() {
		// TODO Auto-generated method stub
		ArrayList<Character> lletres = new ArrayList<Character>();
		
		for (char lletra = 'A'; lletra <= 'Z'; lletra++) {
			lletres.add(lletra);
		}
		
		return lletres;
	}

	/**
	 * Inicialitza un arrayList de Strings amb 10 paraules o més possibles per esbrinar
	 * @return la llista de paraules
	 */
	private static ArrayList<String> carregaParaules() {
		// TODO Auto-generated method stub
		ArrayList<String> llista = new ArrayList<String>();
		
		llista.add("CASA");
		llista.add("TAULA");
		llista.add("CADIRA");
		llista.add("ORDINADOR");
		llista.add("PROGRAMA");
		llista.add("TECLA");
		llista.add("TEMPS");
		llista.add("LLISTA");
		llista.add("TISORES");
		llista.add("ARMARI");
		llista.add("LLETRA");
		
		return llista;
	}

	
}
