package ElPenjat;


import java.util.Hashtable;
import java.util.Random;
import java.util.Scanner;

/**
 * Classe que gestiona els jugadors del penjat:
 * - alta de jugador
 * - baixa de jugador
 * - actualitzar partides guanyades
 * - consultar partides guanyades
 */

/*
 * Ficarem els jugadors en un diccionari.
 * Els mètodes més usuals són:
 * myMap.size(); // Devuelve el numero de elementos del Map
 * myMap.isEmpty(); // Devuelve true si no hay elementos en el Map y false si si los hay
 * myMap.put("68274736E", "Paco Soria"); // Añade un elemento al Map
 * myMap.get("68274736E"); // Devuelve el valor de la clave que se le pasa como parámetro o 'null' si la clave no existe
 * myMap.clear(); // Borra todos los componentes del Map
 * myMap.remove("68274736E"); // Borra el par clave/valor de la clave que se le pasa como parámetro
 * myMap.containsKey("68274736E"); // Devuelve true si en el map hay una clave que coincide con K
 * myMap.containsValue("Paco Soria"); // Devuelve true si en el map hay un Valor que coincide con V
 * myMap.values(); // Devuelve una "Collection" con los valores del Map
 */
public class Jugador {
	
	
	//Creació del diccionari amb els jugadors. Cada element serà (Nom jugador, núm partides guanyades)
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	//Necessitem el canal d'entrada de dades
	static Scanner src = new Scanner(System.in);
	
	/**
	 * Demana el nom del jugador
	 * Dóna d'alta un nou jugador a l'aplicació amb el comptador de partides guanyades a zero.
	 * Si el jugador ja està donat d'alta no farà res
	 * @return String amb el nom del jugador 
	 */
	public static String definirJugador() {
		// TODO Auto-generated method stub
	
		String nom;
		System.out.print("Indica el teu nom de jugador: ");
		nom = src.nextLine();
		if (!jugadors.containsKey(nom)) 
			jugadors.put(nom, 0);
		
		return nom;
	}

	/**
	 * Si el valor rebut per guanya és true, incrementa en 1 el nombre de partides guanyades. En cas contrari no fa res
	 * @param jugador: nom del jugador
	 * @param guanya: true si ha guanyat la partida. False en cas contrari
	 */
	public static void actualitzaResultat(String jugador, boolean guanya) {
		// TODO Auto-generated method stub
		if (jugadors.containsKey(jugador) && guanya == true)
			jugadors.put(jugador, jugadors.get(jugador)+1);
	}

	/**
	 * Mostra el nombre de partides guanyades pel jugador que rep per paràmetre
	 * @param jugador: nom del jugador
	 */
	public static void mostraResultats(String jugador) {
		// TODO Auto-generated method stub
		if (jugadors.containsKey(jugador)) {
			System.out.println("Partides guanyades pel jugador " +
		jugador + ": " + jugadors.get(jugador));
		}
	}

	/**
	 * Mostra per pantalla el nombre de partides guanyades de cada jugador
	 */
	public static void mostrarEstadistica() {
		// TODO Auto-generated method stub
		//Para cada elemento key del conjunto jugadors.keySet()
		for (String key : jugadors.keySet()) {
			System.out.println(key + " => " + jugadors.get(key));
		}
	}

	public static void mostraJugadors(String jugador) {
		// TODO Auto-generated method stub
		
	}
	
}
