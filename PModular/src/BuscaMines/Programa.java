package BuscaMines;

import java.util.Scanner;

public class Programa {
	static Scanner src = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub


		int op = 0;
		boolean resultat;
		boolean definit = false;
		String jugador1= null;

	do {
		op = mostrarMenu();
		
	
		
		switch (op) {
		case 1 : Ajuda.mostrarAjuda();
		break;
		case 2: jugador1= Jugador.definirJugador(jugador1);
				definit = true;
		break;
		case 3: if (definit == true) {
			resultat= Joc.jugarPartida(jugador1);
		} else System.out.println("No pots jugar perque no hi ha un jugador definit");
		break;
		case 0: break;
		
		} 
		
		
		
		
	}while (op!=0);
	
	

	}

	private static int mostrarMenu() {
		// TODO Auto-generated method stub
		int op=0;
		boolean correcte = true;

		System.out.println("---------BUSCAMINES-------------");
		System.out.println("1. MostrarAjuda");
		System.out.println("2. Definir Jugador");
		System.out.println("3. Jugar Partida");
		System.out.println("0. Acabar Programa");
		System.out.println("--------------------------------");
		do {
			System.out.print("Opció: ");
			try {
				op = src.nextInt();
			}
			catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte = false;
			}
			
		} while (correcte == false);

		 
		return op;
	}

}
