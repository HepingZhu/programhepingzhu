package BuscaMines;

import java.util.Random;
import java.util.Scanner;

public class Joc {
	
	static Scanner src = new Scanner(System.in);
	  private static char[][] tablero;
	    private static char[][] tableroVisible;
	    static int filas;
	    private static int columnas;
	    private static int numMinas;


	   public static boolean jugarPartida(String jugador1) {
		    
	    	
	    
	        inicializarTablero(8, 8, 10); // Puedes ajustar el tamaño y el número de minas según tus preferencias   

	        do{
	            mostrarTableroVisible();
	
	            
	            System.out.print("Ingresa la fila: ");
	            int fila = src.nextInt();
	            System.out.print("Ingresa la columna: ");
	            int columna = src.nextInt();

	            if (esJugadaValida(fila, columna)) {
	                if (esMina(fila, columna)) {
	                    System.out.println("¡Has perdido! Una mina explotó.");
	                    mostrarTablero();
	                   
	                } else {
	                    actualizarTableroVisible(fila, columna);
	                    if (esVictoria()) {
	                        System.out.println("¡Felicidades, has ganado!");
	                        mostrarTablero();
	                       
	                    }
	                }
	            } else {
	                System.out.println("Jugada no válida. Inténtalo de nuevo.");
	            }
	        }while (numMinas < 0);

	        src.close();
			return false;
	    }
	    

	    private static void inicializarTablero(int filas, int columnas, int numMinas) {
	        tablero = new char[filas][columnas];
	        tableroVisible = new char[filas][columnas];
	        Joc.filas = filas;
	        Joc.columnas = columnas;
	        Joc.numMinas = numMinas;

	        // Inicializar tablero y tableroVisible
	        for (int i = 0; i < filas; i++) {
	            for (int j = 0; j < columnas; j++) {
	                tablero[i][j] = ' ';
	                tableroVisible[i][j] = '-';
	            }
	        }

	        // Colocar minas aleatorias
	        Random rand = new Random();
	        int minasColocadas = 0;
	        while (minasColocadas < numMinas) {
	            int filaAleatoria = rand.nextInt(filas);
	            int columnaAleatoria = rand.nextInt(columnas);

	            if (tablero[filaAleatoria][columnaAleatoria] != 'X') {
	                tablero[filaAleatoria][columnaAleatoria] = 'X';
	                minasColocadas++;
	            }
	        }
	    }

	    private static void mostrarTableroVisible() {
	        System.out.println("Tablero:");
	        for (int i = 0; i < filas; i++) {
	            for (int j = 0; j < columnas; j++) {
	                System.out.print(tableroVisible[i][j] + " ");
	            }
	            System.out.println();
	        }
	        System.out.println();
	    }

	    private static void mostrarTablero() {
	        System.out.println("Tablero completo:");
	        for (int i = 0; i < filas; i++) {
	            for (int j = 0; j < columnas; j++) {
	                System.out.print(tablero[i][j] + " ");
	            }
	            System.out.println();
	        }
	        System.out.println();
	    }

	    private static boolean esJugadaValida(int fila, int columna) {
	        return (fila >= 0 && fila < filas && columna >= 0 && columna < columnas
	                && tableroVisible[fila][columna] == '-');
	    }

	    private static boolean esMina(int fila, int columna) {
	        return tablero[fila][columna] == 'X';
	    }

	    private static void actualizarTableroVisible(int fila, int columna) {
	        int minasCercanas = contarMinasCercanas(fila, columna);
	        tableroVisible[fila][columna] = (char) (minasCercanas + '0');
	    }

	    private static int contarMinasCercanas(int fila, int columna) {
	        int contador = 0;
	        for (int i = fila - 1; i <= fila + 1; i++) {
	            for (int j = columna - 1; j <= columna + 1; j++) {
	                if (i >= 0 && i < filas && j >= 0 && j < columnas && tablero[i][j] == 'X') {
	                    contador++;
	                }
	            }
	        }
	        return contador;
	    }

	    private static boolean esVictoria() {
	        int casillasSinDescubrir = filas * columnas - numMinas;
	        for (int i = 0; i < filas; i++) {
	            for (int j = 0; j < columnas; j++) {
	                if (tableroVisible[i][j] == '-' && tablero[i][j] != 'X') {
	                    casillasSinDescubrir--;
	                }
	            }
	        }
	        return casillasSinDescubrir == 0;
	    }
	       
	        
		}
	