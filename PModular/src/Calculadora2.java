import java.util.Scanner;

public class Calculadora2 {
	

	static Scanner src = new Scanner(System.in);


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int v1 = 0, v2 = 0;  //valors per operar
		int res = 0;     //guarda el resultat de l'operació
		int op;
		boolean opcio1 = false;
		
		do {
			op = mostrarMenu();
			
			if (opcio1 == false && op>1) {
				System.out.println("Primer cal obtenir els valors de v1 i v2");
			}
			else {
			switch (op) {
			 	case 1: v1 = obtenirNum();
						v2 = obtenirNum();
						opcio1 = true;
						break;
			 	case 2: if (opcio1 == false); 
			 		res = suma(v1, v2);
			 	        break;
			 	case 3: res = resta(v1, v2);
	 	        		break;
			 	case 4: res = multiplicacio(v1, v2);
	 	        		break;
			 	case 5: res = divisio(v1, v2);
	 	        		break;
			 	case 6: mostrarResultat(res);
	     				break;
	     		case 0: System.out.println("Has finalitzat");
	     				break;
	     		default: System.out.println("Error, opció incorrecta");
			}
			}
		} while (op != 0);
	}
	

	//Mostra per pantalla el resultat de l'operació calculada
	//res conté el resultat
	private static void mostrarResultat(int r) {
		// r és el paràmetre que conté el valor de la variable res del main
		System.out.println("El resultat és: " + r);
	}

	private static int divisio(int op1, int op2) {
		// TODO Auto-generated method stub
		char op;
		int res = 0;
		if (op2 == 0) System.out.println("Error, no es pot dividir entre zero");
		else {
			boolean correcte = true;
			do {
				System.out.print("/ or %: ");
				op = src.nextLine().charAt(0);
				switch (op) {
					case '/': res = op1/op2;
						 break;
					case '%': res = op1 % op2;
						 break;
					default: correcte = false;
							 System.out.println("Error, opció incorrecte");
				}
			} while (correcte);
			
		}
		return res;
	}

	private static int multiplicacio(int op1, int op2) {
		// TODO Auto-generated method stub
		return op1 * op2;
	}

	private static int resta(int op1, int op2) {
		// TODO Auto-generated method stub
		return op1 - op2;
	}

	private static int suma(int op1, int op2) {
		// TODO Auto-generated method stub
		int r = op1 + op2;
		return r;
	}

	private static int obtenirNum() {
		// TODO Auto-generated method stub
		
		int v = 0;
		boolean correcte = false;
		
		do {
			System.out.print("Introdueix un valor enter: ");
			try {
				v = src.nextInt();
				correcte = true;
			}
			catch (Exception e) {
				System.out.println("Error, cal introduir un número enter");
				src.nextLine();
			}
		} while (!correcte);
		
		return v;
	}

	//Mostra per pantalla el menú d'opions
	private static int mostrarMenu() {
		// TODO Auto-generated method stub
		int op = 0;
		
		System.out.println("\nCalculadora:\n");
		System.out.println("1.- Obtenir els valors");
		System.out.println("2.- Sumar");
		System.out.println("3.- Restar");
		System.out.println("4.- Multiplicar");
		System.out.println("5.- Dividir");
		System.out.println("6.- Visualitzar Operadors");
		System.out.println("0.- Sortir");
		//System.out.print("\n\nTria una opció: ");
		
		op = obtenirNum();
		
		
		return op;
	}

}
