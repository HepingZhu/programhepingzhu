package Memory;

import java.util.Hashtable;
import java.util.Scanner;	

public class Jugador {

	static Scanner src = new Scanner(System.in);
	
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	
	public static String definirJugador() {
	    // TODO Auto-generated method stub
		    
		    System.out.print("Nombre del Jugador: ");
		    
		    String nombre1 = src.nextLine();
		    jugadors.putIfAbsent(nombre1, 0);

		    	return null ;
		    
		}
	public static void actualitzaResultat(String jugador, boolean guanya) {
		// TODO Auto-generated method stub
		if (jugadors.containsKey(jugador) && guanya == true)
			jugadors.put(jugador, jugadors.get(jugador)+1);
	
	}
	public static void mostraJugadors(String jugador) {	
		// TODO Auto-generated method stub
		if (jugadors.containsKey(jugador)) {
			System.out.println("Partides guanyades pel jugador " +
		jugador + ": " + jugadors.get(jugador));
		}
	}
}
	

