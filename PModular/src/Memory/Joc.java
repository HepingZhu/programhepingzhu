package Memory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Joc {
    static Scanner src = new Scanner(System.in);

    public static boolean jugarPartida(String j1, String j2) {
        int fila1, col1, fila2, col2;

        char[][] oculta;
        char[][] visible;
        final int MAX = 4;
        
        int puntsJ1;
        int puntsJ2;

        boolean fin;

        int torn;

        // Inicializar partida
        oculta = posarLletres(MAX);
        visible = creatTauler(MAX);

        puntsJ1 = puntsJ2 = 0;
        fin = false;
        torn = 1;

        // Jugar partida
        do {
            mostrarEstat(visible, puntsJ1, puntsJ2, torn, MAX);

            fila1 = demanarPos(MAX);
            col1 = demanarPos(MAX);
            destaparCasella(fila1, col1, oculta, visible);

            fila2 = demanarPos(MAX);
            col2 = demanarPos(MAX);
            destaparCasella(fila2, col2, oculta, visible);

            torn = actualizarTauler(fila1, col1, fila2, col2, visible, puntsJ1, puntsJ2, torn, MAX);
            fin = comprovarTauler(puntsJ1, puntsJ2, MAX);

        } while (!fin);

        mostrarEstat(visible, puntsJ1, puntsJ2, torn, MAX);
        return comprovarGuanyador(j1, j2, puntsJ1, puntsJ2);
    }

    private static char[][] creatTauler(int mAX) {
    	 char[][] oculta = new char[mAX][mAX];

    	    for (int i = 0; i < mAX; i++) {
    	        for (int j = 0; j < mAX; j++) {
    	            oculta[i][j] = '?';
    	        }
    	    }

    	    return oculta;
    	}

	private static void mostrarEstat(char[][] visible, int puntsJ1, int puntsJ2, int torn, int mAX) {
        System.out.println("Tablero actual:");
        for (int i = 0; i < mAX; i++) {
            for (int j = 0; j < mAX; j++) {
                System.out.print(visible[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Puntuación - Jugador 1: " + puntsJ1 + " | Jugador 2: " + puntsJ2);
        System.out.println("Turno del Jugador " + torn);
    }
	private static char[][] posarLletres(int mAX) {
		// TODO Auto-generated method stub
    	   Random rd = new Random();
    	    char[][] oculta = new char[mAX][mAX];

    	    char[] letras = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

    	    ArrayList<Character> letrasDuplicadas = new ArrayList<>();
    	    for (char letra : letras) {
    	        letrasDuplicadas.add(letra);
    	        letrasDuplicadas.add(letra);
    	    }

    	    Collections.shuffle(letrasDuplicadas);

    	    for (int i = 0; i < mAX; i++) {
    	        for (int j = 0; j < mAX; j++) {
    	            oculta[i][j] = letrasDuplicadas.remove(0);
    	        }
    	    }

    	    return oculta;
    	}

	private static int demanarPos(int mAX) {
        int pos;
        boolean entradaValida;

        do {
            System.out.print("Introduce la fila y luego la columna (0 a " + (mAX - 1) + "): ");
            pos = src.nextInt();

            entradaValida = pos >= 0 && pos <= mAX;
            
            

            if (!entradaValida) {
                System.out.println("Posición inválida. Debe estar entre 0 y " + (mAX - 1) + ".");
            }

        } while (!entradaValida);

        return pos;
    }

    private static boolean comprovarGuanyador(String j1, String j2, int puntsJ1, int puntsJ2) {
        if (puntsJ1 > puntsJ2) {
            System.out.println("El jugador " + j1 + " ha ganado con " + puntsJ1 + " puntos.");
            return true;
        } else if (puntsJ2 > puntsJ1) {
            System.out.println("El jugador " + j2 + " ha ganado con " + puntsJ2 + " puntos.");
            return true;
        } else {
            System.out.println("¡Empate! Ambos jugadores tienen la misma puntuación.");
            return false;
        }
    }

    private static boolean comprovarTauler(int puntsJ1, int puntsJ2, int mAX) {
        int maxPuntsPosibles = (mAX * mAX) / 2;

        // Verificar si se han encontrado todas las parejas
        return (puntsJ1 + puntsJ2) == maxPuntsPosibles;
    }

    private static int actualizarTauler(int fila1, int col1, int fila2, int col2, char[][] visible, int puntsJ1,
            int puntsJ2, int torn, int mAX) {
        if (posicionsDinsLimits(fila1, col1, fila2, col2, mAX)) {

            if (visible[fila1][col1] == visible[fila2][col2]) {
                if (torn == 1) {
                    puntsJ1++;
                } else {
                    puntsJ2++;
                }
                System.out.println("Puntos actuales - Jugador 1: " + puntsJ1 + " | Jugador 2: " + puntsJ2);
            } else {
                visible[fila1][col1] = '?';
                visible[fila2][col2] = '?';
                System.out.println("Letras diferentes. Turno del otro jugador.");
            }

            torn = (torn == 1) ? 2 : 1;
        } else {
            System.out.println("Posición inválida. No se puede actualizar el tablero.");
        }

        return torn;
    }

    private static boolean posicionsDinsLimits(int fila1, int col1, int fila2, int col2, int mAX) {
        return fila1 >= 0 && fila1 < mAX && col1 >= 0 && col1 < mAX &&
               fila2 >= 0 && fila2 < mAX && col2 >= 0 && col2 < mAX;
    }

    private static void destaparCasella(int fila1, int col1, char[][] oculta, char[][] visible) {
        if (posicionsDinsLimits(fila1, col1, oculta.length, oculta[0].length, col1)) {
            // Verificar que la posición no esté destapada
            if (visible[fila1][col1] == '?') {
                // Revelar la letra en la posición especificada
                visible[fila1][col1] = oculta[fila1][col1];
            } else {
                System.out.println("La casilla ya está destapada. Elige otra posición.");
            }

        }
    }
}

	
