package Memory;

import java.util.Scanner;

public class Programa {

	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int op = 0;
		
		
		
		do {
			op = mostrarMenú();
			
			String jugador = null;
			boolean definit= true;
			switch (op) {
				case 1: Ajuda.mostrarAjuda();
						break;
				case 2:
				    if (definit) {
				        String jugador1 = Jugador.definirJugador();
				        String jugador2 = Jugador.definirJugador();
				        boolean resultat = Joc.jugarPartida(jugador1, jugador2);
				        Jugador.actualitzaResultat(jugador1, resultat);
				        definit = true;
				    } else {
				        System.out.println("No puedes jugar hasta que los jugadores estén definidos.");
				    }
				    break;

				case 3: Jugador.mostraJugadors(jugador);
						break;
				case 0: System.out.println("Fi de joc");
						break;
				default: System.out.println("Opció incorrecta");
			}
			
		} while (op != 0);
	}

	private static int mostrarMenú() {
		// TODO Auto-generated method stub
		int op = 0; 
		boolean correcte = true;
		
		System.out.println("-----------------Memory------------------");
		System.out.println("1. Mostrar Ajuda");
		System.out.println("2. Jugar partida");
		System.out.println("3. Mostra jugador");
		System.out.println("0. Sortir");
		System.out.println("-----------------------------------------");
		do {
			System.out.print("Opció: ");
			
			try {
				op = src.nextInt();
			}
			catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte = false;
			}
			
		} while (correcte == false);
		
		//src.close();
		
		return op;
	}

}
