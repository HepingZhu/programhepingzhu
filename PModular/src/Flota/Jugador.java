package Flota;

import java.util.Scanner;

public class Jugador {

		public static void jugarPartida(String jugador1, String jugador2) {
			// TODO Auto-generated method stub
			
			
	        char[][] tablero = inicializarTablero();
	        int resultado = 0; 

	        Scanner src = new Scanner(System.in);

	        while (resultado == 0) {
	            
	            imprimirTablero(tablero, jugador1);
	            realizarAtaque(tablero, jugador1);

	            
	            resultado = verificarGanador(tablero);
	            if (resultado != 0) break;

	           
	            imprimirTablero(tablero, jugador2);
	            realizarAtaque(tablero, jugador2);

	            
	            resultado = verificarGanador(tablero);
	        }

	        
	        if (resultado == 1) {
	            System.out.println(jugador1 + " ha ganado la partida.");
	        } else {
	            System.out.println(jugador2 + " ha ganado la partida.");
	        }

	        src.close();
	    }

	    private static char[][] inicializarTablero() {
	   
	        char[][] tablero = new char[10][10];
	        for (char[] fila : tablero) {
	            for (int i = 0; i < fila.length; i++) {
	                fila[i] = '~';
	            }
	        }
	        return tablero;
	    }

	    private static void imprimirTablero(char[][] tablero, String jugador) {
	        System.out.println("Tablero de " + jugador + ":");
	        for (char[] fila : tablero) {
	            for (char casilla : fila) {
	                System.out.print(casilla + " ");
	            }
	            System.out.println();
	        }
	        System.out.println();
	    }

	    private static void realizarAtaque(char[][] tablero, String jugador) {
	        Scanner src = new Scanner(System.in);

	        System.out.println(jugador + ", es tu turno. Realiza un ataque.");
	        System.out.print("Fila: ");
	        int fila = src.nextInt();
	        System.out.print("Columna: ");
	        int columna = src.nextInt();


	        if (tablero[fila][columna] == '~') {
	            System.out.println("Agua!");
	            tablero[fila][columna] = 'o';  
	        } else {
	            System.out.println("¡Tocado!");
	            tablero[fila][columna] = 'X';  
	        }
	    }

	    private static int verificarGanador(char[][] tablero) {
	      
	        for (char[] fila : tablero) {
	            for (char casilla : fila) {
	                if (casilla == 'o') {
	                    return 0;  
	                }
	            }
	        }
	        return 1;  
	    }
	}


