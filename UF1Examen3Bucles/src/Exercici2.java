import java.util.Scanner;

public class Exercici2 {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        while (src.hasNext()) {
            String numCasos = src.next();

           
            while (numCasos.equals("FI")){
                
            

            int numPartits = Integer.parseInt(numCasos);

          
            String parejaGanadora = "";
            int maxSetsGanados = -1;
            boolean empate = false;

           
            for (int i = 0; i < numPartits; i++) {
                String nombreLocal = src.next();
                int setsLocal = src.nextInt();
                String nombreVisitante = src.next();
                int setsVisitante = src.nextInt();

        
                int totalSetsLocal = setsLocal + setsVisitante;
                int totalSetsVisitante = setsVisitante + setsLocal;

            
                if (totalSetsLocal > maxSetsGanados) {
                    maxSetsGanados = totalSetsLocal;
                    parejaGanadora = nombreLocal;
                    empate = false;
                } else if (totalSetsVisitante > maxSetsGanados) {
                    maxSetsGanados = totalSetsVisitante;
                    parejaGanadora = nombreVisitante;
                    empate = false;
                } else if (totalSetsLocal == maxSetsGanados || totalSetsVisitante == maxSetsGanados) {
                    empate = true;
                }
            }

         
            if (empate) {
                System.out.println("EMPAT");
            } else {
                System.out.println(parejaGanadora);
            }
        }
        }

        
        
    }
}
    
