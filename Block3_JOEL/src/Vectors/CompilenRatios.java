package Vectors;

import java.util.Scanner;

public class CompilenRatios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		
		for (; casos > 0; casos--) {
			int total = src.nextInt();
			src.nextLine();
			String [] alumnes = src.nextLine().split(" ");
			int pos = src.nextInt();
			
			for (int i = 0; i < total - 1; i++) {
				if (i != pos) System.out.print(alumnes[i] + " ");
			}
			if (pos != total -1) System.out.println(alumnes[total-1]);
		}
	}

}

