package Vectors;

import java.util.Scanner;

public class CambialoUnPoco {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int n = src.nextInt();
			int[]array = new int[n];
				
			for(int pos = 0; pos < n; pos++) {
					array[pos]= src.nextInt();
			}
			int v1 = src.nextInt();
			int v2 = src.nextInt();
			
			int pos = 0;
	
			while (pos < n){
				if (array[pos] == v1 ) {
					array [pos] = v2;		
				}
				pos++;
			}	
			pos=0;
			while (pos < n){
				System.out.print(array[pos]+" ");
				pos++;
			}	
			System.out.println();
		}		
	}
}
