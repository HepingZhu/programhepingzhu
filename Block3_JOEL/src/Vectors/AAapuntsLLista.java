package Vectors;

import java.util.ArrayList;
import java.util.Scanner;

public class AAapuntsLLista {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Scanner src = new Scanner(System.in);
		
		//Declaració de la variable ArrayList
		ArrayList<Integer> arrLista = new ArrayList<Integer>();
		
		//Afegir valors a l'arrsyList
		arrLista.add(4);
		
		//Obtenir el nombre d'elements de la llista
		int n = arrLista.size();
		
		//Obtenir el valor de la llista a la posició i
		int i = 4;
		int valor = arrLista.get(i);
		
		//Preguntar si un valor està dins de la llista
		if (arrLista.contains(3)) {
			//....
		}
		
		//Saber la posició d'un element dins de la llista
		int pos = arrLista.indexOf(valor);
		
		//posició que ocupa l'últm element de la llista amb aquest valor
		pos = arrLista.lastIndexOf(valor);
		
		//Esborrar l'element d'una posició concreta (pos)
		arrLista.remove(pos);
		
		//Esborrar l'element amb valor concret
		arrLista.remove((Integer)valor);
		
		//Elimina tots els elements de la llista
		arrLista.clear();
		
		//Saber si la llista està buiida
		if (arrLista.isEmpty()) {
			//...
		}
		
		//Modificar un valor que hi ha dins de l'arrayList
		arrLista.set(pos, valor);
		
		//Copiar un arrayList sobre un altre
		ArrayList<Integer> arrayListCopia = (ArrayList<Integer>) arrLista.clone(); 
		
		//Passar l'arrayList a una array
		Object [] array = arrLista.toArray();  
	}

}

