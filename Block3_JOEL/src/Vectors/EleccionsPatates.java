package Vectors;

import java.util.Scanner;

public class EleccionsPatates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 Scanner src = new Scanner(System.in);
	        int Casos = src.nextInt();

	        for (int i = 0; i < Casos; i++) {
	            int Opcions = src.nextInt();
	            int[] vots = new int[Opcions];

	            for (int j = 0; j < Opcions; j++) {
	                vots[j] = src.nextInt();
	            }

	            int ganador = 0;
	            int maxVots = vots[0];

	            for (int j = 1; j < Opcions; j++) {
	                if (vots[j] > maxVots) {
	                    maxVots = vots[j];
	                    ganador = j;
	                }
	            }

	            System.out.println(ganador + 1);
	        }

	        src.close();
	    }
	}

	      