package Vectors;

import java.util.Scanner;

public class IndexOf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		for ( int casos=src.nextInt(); casos>0; casos--) {
			int n = src.nextInt();
			int [] array = new int [n];
			
			for (int pos = 0; pos <n; pos++) {
				array [pos] = src.nextInt();
			}
			int element = src.nextInt();
			
			boolean trobat = false;
			int i =0;
			
			while (i<n && trobat == false) {
				if( array[i]==element) {
					trobat= true;
				}
				else {
					i++;
				}
			}
			if(trobat==true) {
				System.out.println(i);
			}
			else {
				System.out.println("-1");
			}
		}
	}

}
