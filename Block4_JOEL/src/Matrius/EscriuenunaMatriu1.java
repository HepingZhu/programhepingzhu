package Matrius;

import java.util.Scanner;

public class EscriuenunaMatriu1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		final int FILES = src.nextInt(); // final es que no tienen que variar
		final int COLS = src.nextInt();
		
		int [] [] mat = new int [FILES][COLS];
		
		for (int i=0; i<FILES; i++) { // per cada fila
			
			for (int j=0; j<COLS; j++){ // Per cada sella dins de la fila
				
				mat[i][j]=src.nextInt();
		}
		
	}
		
		//Llegim la pos de la casella a consultar
		
		int ii=src.nextInt();
		int jj=src.nextInt();
		
		//mostren el contingunt de la matriu
		for (int i=0; i<FILES; i++) { // per cada fila
				
				for (int j=0; j<COLS; j++){ // Per cada sella dins de la fila
					
						System.out.print(mat[i][j]+" ");
				}
				System.out.println();
			}
		
		//mostem el valor guardat en (ii,jj)
		System.out.println(mat[ii][jj]);

	}
}