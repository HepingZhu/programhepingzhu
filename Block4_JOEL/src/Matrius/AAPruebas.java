package Matrius;


import java.util.Scanner;

public class AAPruebas {
    public static void main(String[] args) {
    	Scanner src = new Scanner(System.in);

        char[][] mat = new char[8][8];

        int casos = src.nextInt();

        for (int caso = 0; caso < casos; caso++) {
            // Leer la posición en notación algebraica, por ejemplo, "e5".
            String posStr = src.next();
            int row = 8 - Character.getNumericValue(posStr.charAt(1));
            int col = posStr.charAt(0) - 'a';

            // Marcar las posiciones atacadas por el alfil.
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (Math.abs(row - i) == Math.abs(col - j)) {
                        mat[i][j] = 'X';
                    }
                }
            }

            // Contar las posiciones atacadas por el alfil.
            int cont = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (mat[i][j] == 'X') {
                        cont++;
                    }
                }
            }

            // Imprimir el resultado.
            System.out.println(cont-1);

            // Reiniciar la matriz para el próximo caso.
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    mat[i][j] = '\0';
                }
            }
        }
    }
}
