package Matrius;

import java.util.Scanner;

public class EleccionsalConsellEscolar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int num = src.nextInt();
		char [][] mat = new char[num][num];
		
		for (int j = 0; j < num; j++) {
			mat [0][j] = 'X';
			mat [num-1][j] = 'X';
		}
		for (int i = 0; i < num; i++) {
			mat [i][0] = 'X';
			mat [i][num-1] = 'X';
		}
		
		for (int i = 1; i < num -1; i++) {
			for (int j = 1; j < num-1; j++) {
				if (i == j) mat[i][j] = 'X';
				else if (j + i + 1 == num) mat [i][j] = 'X';
				else mat[i][j] = '.';
			}
		}
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				System.out.print(mat[i][j]);
			}
			System.out.println();
		}
	}

}
