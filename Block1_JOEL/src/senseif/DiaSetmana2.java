package senseif;

import java.util.Scanner;

public class DiaSetmana2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		  Scanner scanner = new Scanner(System.in);
	        
	        while (scanner.hasNextInt()) {
	            int N = scanner.nextInt();
	            String[] daysOfWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	            
	            // Asegúrate de que N esté dentro del rango válido
	            if (N >= 1 && N <= 99999) {
	                // Calcula el día de la semana usando el módulo 7
	                int dayIndex = N % 7;
	                System.out.println(daysOfWeek[dayIndex]);
	            }
	        }
	        
	        scanner.close();
	    }
	}