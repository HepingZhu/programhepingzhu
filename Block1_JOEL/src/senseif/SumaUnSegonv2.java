package senseif;

import java.util.Scanner;

public class SumaUnSegonv2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Scanner src = new Scanner(System.in);
		int hora = src.nextInt();
		int minut = src.nextInt();
		int segon = src.nextInt();
		
		segon++; // segon = segon + 1; //segon += 1;
		if (segon == 60) {
		    segon = 0;
		    minut = minut + 1;
		}
		
		if (minut == 60) {
		    minut = 0;
		    hora = hora + 1;
		}
		
		if (hora == 24) 
		    hora = 0;
		    
	
		System.out.println(hora+" "+minut+" "+segon);
		
	}

}
