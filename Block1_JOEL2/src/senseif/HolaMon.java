package senseif;

import java.util.Scanner;

public class HolaMon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Això és im cp,emtaro de vàries línies
		 * Aquí continua
		 */
		String nom;  //variable on guardem el nom de la perosna que llegirem per teclat
		Scanner src = new Scanner(System.in);   		//src és el canal d'entrada de dades


		
		//Exemples de diferents tipus de dades simples o primitives
		int numero; //enter
		char caracter;
		double real;	//real
		boolean b;	//Booleà
		
		
		System.out.print("Número enter: ");
		numero = src.nextInt();
		System.out.print("Número real: ");
		real = src.nextDouble();		
		src.nextLine();					//neteja de buffer d'entrada
		System.out.print("Número caràcter: ");
		caracter = src.nextLine().charAt(0); 		//llegeix un caràcter i ho guarda a la variable
		
		b = true;
		b = !b && false; 
		
		numero = 3;
		caracter = 'a';
		real = 4.6;
		b = true;
		
		numero = numero * 10;
		real = numero;
		b = !b && false; 
		
		System.out.println("Resultats de les variables que fem servir\n" +
							"numero = " + numero + "\n" + 
							"caracter = " + caracter + "\n" + 
							"real = " + real + "\n" + 
							"b = " + b);
							
		
		
		System.out.print("Introdueix el teu nom: ");
		nom = src.nextLine(); 		//nextLine() legeix de l'entrada tots el caràcters fins trobar un <Intro>
		
		
		System.out.println("Houla, " + nom + ", \n\n\ncargando..."); 

	}

}

