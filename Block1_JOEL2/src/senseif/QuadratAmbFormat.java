package senseif;

import java.util.Scanner;

public class QuadratAmbFormat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner src = new Scanner(System.in);
		
		Double costat = src.nextDouble();
		Double area = costat * costat;
		
		System.out.printf("%015.3f\n", area); // va con comas (,) los numeros
		
	}

}
