import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercici1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 Scanner src = new Scanner (System.in);
		
		 System.out.print("Ingresa una frase: ");
		 String frase = src.nextLine();

	         if (!frase.contains("*")) {
	     System.out.println("No ha fet servir la negreta");
	     
	       } else { 
	      Pattern pattern = Pattern.compile("\\*([^*]+)\\*");
	      Matcher matcher = pattern.matcher(frase);

	         if (matcher.matches()) {
	                System.out.println("Tota la frase està en negreta");
	                
	        } else if (frase.startsWith("*") && frase.endsWith("*")) {
	       System.out.println("Alguna part de la frase està en negreta");
	                
	         } else {
	      boolean encontrado = false;
	       
	      while (matcher.find()) {
	      String parteEnNegrita = matcher.group(1);
	      System.out.println("Alguna part de la frase està en negreta: " + parteEnNegrita);
	       
	      encontrado = true;
	    }

	         if (!encontrado) {
	      System.out.println("Negreta no correcte");
	                } 
	            }
	        }
	        }
		 
	}
	