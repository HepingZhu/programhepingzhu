import java.util.Scanner;

public class Exercici2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner(System.in);

		 System.out.print("Ingresa la hora (0-23): ");
	        int horas = src.nextInt();
	        if (horas < 0 || horas > 23) {
	           System.out.println("Error: Hora incorrecta.");
	            return;
	        }
	        
	        System.out.print("Ingresa el minuto (0-60): ");
	        int min = src.nextInt();
	        if (min < 0 || min > 60) {
	           System.out.println("Error: Hora incorrecta.");
	            return;
	        }
	        
	        System.out.print("Ingresa el seg (0-60): ");
	        int seg = src.nextInt();
	        if (seg < 0 || seg > 60) {
	           System.out.println("Error: Hora incorrecta.");
	            return;
	        }
	        System.out.print("Ingresa un número del 1 al 10: ");
	        int num = src.nextInt();
	        if (num < 1 || num > 10) {
	            System.out.println("Error");
	            return;
	        }
	        seg += num;

	      
	        if (seg >= 60) {
	            seg -= 60;
	            min++;
	        }
	        if (min >=60) {
	        	min -=60;
	        	horas++;
	        }
	        if (horas >=24) {
	        	horas =0;
	        	
	        }
		
	        System.out.println("Hora: " + horas + ":" + min + ":" + seg);
	}

}
