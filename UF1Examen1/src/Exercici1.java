import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		char c1 = src.nextLine().charAt(0);
		char c2 = src.nextLine().charAt(0);
		char c3 = src.nextLine().charAt(0);
		
		if (c1 == 'G' && c2 == 'N' && c3 == 'N') {
			System.out.println("Magenta");
		}
		else if (c1 == 'M' && c2 == 'G' && c3 == 'N') {
			System.out.println("Vermell");
		}
		else if (c1 == 'M' && c2 == 'G' && c3 == 'C') {
			System.out.println("Negre");
		}
		else if (c1 == 'N' && c2 == 'G' && c3 == 'N') {
			System.out.println("Groc");
		}
		else if (c1 == 'N' && c2 == 'G' && c3 == 'C') {
			System.out.println("Verd");
		}
		else if (c1 == 'N' && c2 == 'N' && c3 == 'C') {
			System.out.println("Cian");
		}
		else if (c1 == 'M' && c2 == 'N' && c3 == 'C') {
			System.out.println("Violeta");
		}
		else if (c1 == 'N' && c2 == 'N' && c3 == 'N') {
			System.out.println("Blanc");
		}
	}

}
