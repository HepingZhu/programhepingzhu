
public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Empleat e1 = new Empleat();
		Empleat e2 = new Empleat();
		
		Departament d1 = new Departament();
		Departament d2 = new Departament();
		
		e1.nom = "Javier";
		e1.cognom = "Yoya";
		e1.edat = 23;
		e1.mostrar();
		
		System.out.println();
		
		e2.nom = "Koldo";
		e2.cognom = "Perez";
		e2.edat = 20;
		e2.mostrar();
		
		
		System.out.println();
		
		d1.nom= "Officinas de KOI";
		d1.Ubicacio = "Madrid";
		d1.cap= e2;
		d1.mostrar();
		
		System.out.println();
		
		d2.nom= "MSI Office";
		d2.Ubicacio = "EEUU";
		d2.cap= e1;
		d2.mostrar();
	}

}
