import java.util.Scanner;

public class Exercici3 {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();

        for (int i = 0; i < casos; i++) {

            String sup = src.nextLine();
            String inf = src.nextLine();

            boolean encajar = true;

            for (int j = 0; j < sup.length(); j++) {
                if (sup.charAt(j) == '2' && inf.charAt(j) == '1') {
                    encajar = false;
                   
                }
            }

            if (encajar) {
                System.out.println("SI");
            } else {
                System.out.println("NO");
            }
        }

        
    }
}
   
