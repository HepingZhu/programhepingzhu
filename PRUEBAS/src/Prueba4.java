import java.util.Random;
import java.util.Scanner;

public class Prueba4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		
		int c = rd.nextInt(6, 14);
		String matriu[][] = new String[c][c];
		
		for (int i = 0; i < c; i++) {
			for (int j = 0; j < c; j++) {
				if (i == j) {
					matriu[i][j] = "A";
				} else if (i > j) {
					matriu[i][j] = "D";
				} else {
					matriu[i][j] = "M";
				}
			}
		}
		
		for (int i = 0; i < c; i++) {
			for (int j = 0; j < c; j++) {
				System.out.print(matriu[i][j] + " ");
			}
			System.out.println();
		}
		sc.close();
	}
}