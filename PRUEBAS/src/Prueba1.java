import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Prueba1{import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Jugar {

    private static Scanner scanner = new Scanner(System.in);
    private static char[][] tablero = new char[8][8];
    private static final int MAX_MOVIMIENTOS = 64;
    private static Map<String, Integer> victoriasPorJugador = new HashMap<>();

    public static void main(String[] args) {
        String jugador1 = ObtenirNom(1);
        String jugador2 = ObtenirNom(2);

        boolean resultado = jugarPartida(jugador1, jugador2);

        if (resultado) {
            registrarVictoria(jugador1);
        } else {
            registrarVictoria(jugador2);
        }

        mostrarJugadorExpert();
    }

    public static String ObtenirNom(int numeroJugador) {
        System.out.print("Introduce el nombre del Jugador " + numeroJugador + ": ");
        return scanner.nextLine();
    }

    public static boolean jugarPartida(String jugador1, String jugador2) {
        // ... (resto del código de jugarPartida)
    }

    public static void jugadorExpert(String jugador) {
        // TODO: Implementación del método
    }

    private static void registrarVictoria(String jugador) {
        victoriasPorJugador.put(jugador, victoriasPorJugador.getOrDefault(jugador, 0) + 1);
    }

    private static void mostrarJugadorExpert() {
        if (victoriasPorJugador.isEmpty()) {
            System.out.println("No hay ningún jugador registrado.");
        } else {
            String jugadorExpert = determinarJugadorExpert();
            System.out.println("El jugador experto es: " + jugadorExpert);
        }
    }

    private static String determinarJugadorExpert() {
        String jugadorExpert = null;
        int maxVictorias = 0;

        for (Map.Entry<String, Integer> entry : victoriasPorJugador.entrySet()) {
            if (entry.getValue() > maxVictorias) {
                maxVictorias = entry.getValue();
                jugadorExpert = entry.getKey();
            }
        }

        return jugadorExpert;
    }
}
}