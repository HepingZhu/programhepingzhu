
import java.util.Scanner;

public class Prueba2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);

		  public static boolean jugarPartida(String j1, String j2) {
		        int fila1, col1, fila2, col2;

		        char[][] oculta;
		        char[][] visible;
		        final int MAX = 4;
		        
		        int puntsJ1;
		        int puntsJ2;

		        boolean fin;

		        int torn;

		        // Inicializar partida
		        oculta = posarLletres(MAX);
		        visible = creatTauler(MAX);

		        puntsJ1 = puntsJ2 = 0;
		        fin = false;
		        torn = 1;

		        // Jugar partida
		        do {
		            mostrarEstat(visible, puntsJ1, puntsJ2, torn, MAX);

		            fila1 = demanarPos(MAX);
		            col1 = demanarPos(MAX);
		            destaparCasella(fila1, col1, oculta, visible);

		            fila2 = demanarPos(MAX);
		            col2 = demanarPos(MAX);
		            destaparCasella(fila2, col2, oculta, visible);

		            torn = actualizarTauler(fila1, col1, fila2, col2, visible, puntsJ1, puntsJ2, torn, MAX);
		            fin = comprovarTauler(puntsJ1, puntsJ2, MAX);

		        } while (!fin);

		        mostrarEstat(visible, puntsJ1, puntsJ2, torn, MAX);
		        return comprovarGuanyador(j1, j2, puntsJ1, puntsJ2);
		    }
