import java.util.Random;
import java.util.Scanner;

public class Prueba3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);

		final int f = 8;
		final int c = 10;
		final char buit = '-';
		final char ocupat = 'D';
		char[][] clase = new char[f][c];

		int opcio = -1;

		while (opcio != 0) {
			System.out.println("1.- Buidar Aula");
			System.out.println("2.- Visualitzar Sala");
			System.out.println("3.- Ficar dammys");
			System.out.println("4.- Lider dammy");
			System.out.println("5.- Vulnerable");
			System.out.println("0.- Sortir");
			opcio = sc.nextInt();
			switch (opcio) {
			case 1:
				for (int i = 0; i < f; i++)
					for (int j = 0; j < c; j++)
						clase[i][j] = buit;
				break;
			case 2:
				for (int i = 0; i < f; i++) {
					for (int j = 0; j < c; j++)
						System.out.print(clase[i][j] + " ");
					System.out.println();
				}
				break;
			case 3:
				int num = sc.nextInt();
				int nPlaces = 0;

				Random rd = new Random();
				int comptdum = 0;
				int comptfila = 0;
				int comptcol = 0;
				int colanterior = 0;

				for (int i = 0; i < f; i++) {
					int cc = rd.nextInt(c);
						comptfila = 0;
					if (cc == colanterior)
						comptcol++;
					
					if (comptcol >= 4)
						cc = rd.nextInt(c);
					
					colanterior = cc;
					
					for (int j = 0; j < c; j++) {
						int fc = rd.nextInt(f);
						if (comptdum > num) {
							break;
						}
						if (clase[fc][cc] == buit && comptfila <= 3) {
							clase[fc][cc] = ocupat;
							comptfila++;
							comptdum++;
						}

					}
				}

				break;
			case 4:
				int compt = 0;
				int filamax = 0;
				for (int i = 0; i < f; i++) {
					compt = 0;
					for (int j = 0; j < c; j++) {
						if (clase[i][j] == ocupat) {
							compt++;
						}
						if (i > filamax)
							filamax = i;
					}
				}
				int sol = 0;
				for (int k = 0; k < f; k++) {
					if (clase[filamax][k] == ocupat && k != 0 && k != 9 && k + 1 != ocupat && k - 1 != ocupat) {
						sol = clase[filamax][k];
					}
				}
				System.out.println(sol / 2);
				break;
			case 0:
				System.out.println("Adeu");
				System.exit(0);
			}

		}
		sc.close();
	}
}
