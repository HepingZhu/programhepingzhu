
public class a {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		import java.util.Scanner;

		public class SymmetricNumbers {
		    public static void main(String[] args) {
		        Scanner scanner = new Scanner(System.in);

		        while (true) {
		            System.out.print("Ingrese un número (0 para salir): ");
		            String input = scanner.nextLine();

		            if (input.equals("0")) {
		                System.out.println("Saliendo...");
		                break;
		            }

		            int length = input.length();
		            boolean isSymmetric = true;

		            for (int i = 0; i < length / 2; i++) {
		                if (input.charAt(i) != input.charAt(length - 1 - i)) {
		                    isSymmetric = false;
		                    break;
		                }
		            }

		            if (isSymmetric) {
		                System.out.println("Sí, el número comienza y termina simétricamente.");
		            } else {
		                System.out.println("No, el número no comienza y termina simétricamente.");
		            }
		        }
		    }
		}
