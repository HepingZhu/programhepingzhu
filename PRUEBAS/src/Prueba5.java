import java.util.Random;
import java.util.Scanner;

public class Prueba5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		Random rd = new Random();
		
		System.out.println("Introdueix el tamany del pati: ");
		int f = sc.nextInt();
		int c = sc.nextInt();
		String jardi[][] = new String[f][c];

		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				jardi[i][j] = "O";
			}
		}

		int tresori = rd.nextInt(0, f);
		int tresorj = rd.nextInt(0, c);

		jardi[tresori][tresorj] = "T";
		
		System.out.println("Quants intents vols tenir? ");
		int nintents = sc.nextInt();
		int x;
		int y;
		int rond = 0;
		for (int n = 0; n < nintents; n++) {
			x = sc.nextInt();
			y = sc.nextInt();
			if (jardi[x][y].equals("T")) {
				break;
			}
			rond++;
			jardi[x][y] = "E";
			System.out.println("Has fallat ");
			System.out.println("Intent: " + rond);
		}
		if (rond < nintents) {
			System.out.println("Molt be!! Has guanyat!!");
			System.out.println();
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < c; j++) {
					System.out.print(jardi[i][j] + " ");
				}
				System.out.println();
			}
		} 
		
		else {
			System.out.println("No l'has trobat");
			System.out.println();
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < c; j++) {
					System.out.print(jardi[i][j] + " ");
				}
				System.out.println();
			}
		}
		sc.close();
	}

}