import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
@RunWith(Parameterized.class)
class LaCalculadoraTest {

	@Test
	public void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30,  res);
	 // fail("Not yet implemented");
	}



	@Test	
	public void testResta() {
		int res = LaCalculadora.resta(20, 10);
		assertEquals(10, res);
		// fail("No yet imp..");
		
	}
	
	@Test	
	public void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(8, 2);
		assertEquals(16, res);
		// fail("No yet imp..");
	}
	
	@Test	
	public void testDivisio() {
		int res = LaCalculadora.divideix(12, 1);
		assertEquals(12, res);
		// fail("No yet imp..");
	}
	@Test
	public void testException() {
		int res;
		try {
			res = LaCalculadora.divideix(10, 0);
			//fail("Fallo: Passa per aquí si no es llança l’excepció ArithmeticException");
		}
		catch (ArithmeticException e) {
			//La prova funciona correctament
		}
		

	}
	@Test
	void testExpectedException() {
	 
	  Assertions.assertThrows(ArithmeticException.class, () -> {
	     LaCalculadora.divideix(10, 0);
	  });
	 
	}
	

}