package MasterMind;

import java.util.Scanner;
/**@since Classe Jugar per que el joc del master mind funcioni
 * @author Heping
 * @version 30/1/2024
 */
public class Programa {

	static Scanner src = new Scanner(System.in);
	/**
	 * Método principal que inicia la ejecución del juego MasterMind.
	 *
	 * Este método presenta un bucle que muestra un menú, solicita al usuario que elija una opción
	 * y realiza la acción correspondiente. El juego incluye la opción de definir un jugador y
	 * jugar una partida. El bucle continúa hasta que el usuario elige salir (opción 0).
	 *
	 * @param args Los argumentos de la línea de comandos (no se utilizan en este caso).
	 * @since 1.0
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int op = 0;  //guarda l'opció escollida per l'usuari
		String jugador = null;  //guarda el nom del jugador actual
		boolean resultat;
		boolean definit = false;
		
		do {
			op = mostrarMenu();
			switch (op) {
			case 1: jugador = Jugar.definirJugador();
				definit = true;
				break;
			case 2 : if (definit == true) {
				resultat = Jugar.jugarPartida(jugador);
				definit = false;
			}
			else System.out.println("No pots jugar fins que no estigui definit el jugador");
			break;
			case 0: System.out.println("Adeu");
				break;
			default: System.out.println("Opció incorrecta!");
			}
			
			
			
		} while (op != 0);
	}
	
	/**
	 * Muestra el menú del juego MasterMind y solicita al usuario que elija una opción.
	 *
	 * Este método muestra un menú con opciones específicas del juego y solicita al usuario
	 * que ingrese el número de la opción deseada. Realiza un manejo de excepciones para
	 * garantizar que la entrada del usuario sea válida.
	 *
	 * @return El número de la opción elegida por el usuario.
	 * @since 1.0
	 */
	static int mostrarMenu() {
		// TODO Auto-generated method stub
		
		int op = 0; 
		boolean correcte = true;
		System.out.println(" ------Miser Mind------ ");
		System.out.println("| 1. Definir jugador   |");
		System.out.println("| 2. Jugar partida     |");
		System.out.println("| 0. Acabar programa   |");
		System.out.println("|______________________|");
		
		do {
			System.out.print("Opció: ");
			try {
				op = src.nextInt();
			}
			catch (Exception e) {
				System.out.println("Error, opció no vàlida");
				src.nextLine();
				correcte = false;
			}
			
		} while (correcte == false);	

		return op;
	}

}
