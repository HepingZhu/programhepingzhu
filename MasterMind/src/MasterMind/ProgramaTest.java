package MasterMind;

import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import static org.junit.jupiter.api.Assertions.*;



class ProgramaTest {


    @Test
    public void testMostrarMenu() {
        // Simulamos entrada de usuario para probar las diferentes opciones del menú
        InputStream sysInBackup = System.in; // Backup del System.in original
        ByteArrayInputStream in = new ByteArrayInputStream("1\n".getBytes()); // Simulamos entrada 1 seguido de un salto de línea
        System.setIn(in);

        assertEquals(1, Programa.mostrarMenu());

        // Restauramos el System.in original
        System.setIn(sysInBackup);
    }

    @Test
    public void testMain() {
        // Simulamos entrada de usuario para probar el flujo del programa principal
        InputStream sysInBackup = System.in; // Backup del System.in original
        ByteArrayInputStream in = new ByteArrayInputStream("1\n2\n0\n".getBytes()); // Simulamos entrada 1, luego 2 y finalmente 0 seguido de saltos de línea
        System.setIn(in);

        // No hay una forma directa de probar el método main(), así que simplemente comprobamos que no lanza excepciones
        assertDoesNotThrow(() -> Programa.main(null));

 
        System.setIn(sysInBackup);
    }

}
