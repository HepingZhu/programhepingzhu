package MasterMind;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
/**@since Classe Jugar per poder jugar al joc del master mind
 * @author Heping
 * @version 30/1/2024
 */



public class Jugar {
	

	static Scanner src = new Scanner(System.in);
	static Random rnd = new Random();

	/**
	 * Solicita al usuario que introduzca su nombre de jugador.
	 * Este método muestra un mensaje en la consola pidiendo al usuario que indique su nombre de jugador.
	 * Luego, lee la entrada del usuario desde la consola y devuelve el nombre proporcionado.
	 * @return El nombre del jugador ingresado por el usuario.
	 * @since 1.0	
	 */
	public static String definirJugador() { //Definim el nom del Jugador
		// TODO Auto-generated method stub
		String nom;
		System.out.print("Indica el teu nom de jugador: ");
		nom = src.nextLine();
		return nom;
	}
/**
	 * Inicia una nueva partida del juego MasterMind para un jugador dado.
	 *
	 * Este método genera una combinación aleatoria de cuatro números.
	 * Luego, solicita al jugador que introduzca su tirada y proporciona retroalimentación
	 * sobre el número de aciertos y plenos en la tirada.
	 * El juego continúa hasta que el jugador decide finalizar o gana la partida.
	 *
	 * @param jugador El nombre del jugador que participa en la partida.
	 * @return `true` si el jugador desea continuar jugando, `false` si decide finalizar.
	 * @since 1.0
	 */
	public static boolean jugarPartida(String jugador) {
	    boolean res = true;
	    int intents = 0;
	    	int[] combinacioPrograma = generarVec();
	    do {
	   
	        // Per veure els numeros
	        // System.out.print("Combinació del programa: ");
	        for (int numero : combinacioPrograma) {
	          
	         // System.out.print(numero );
	        }
	        // System.out.println();

	        System.out.print("Tirada de l'usuari: ");
	        String tiradaUsuariStr = src.nextLine();
	        intents++;

	        int[] tiradaUsuari = new int[4];
	        for (int i = 0; i < 4; i++) {
	            tiradaUsuari[i] = Character.getNumericValue(tiradaUsuariStr.charAt(i));
	        }

	        int encerts = contarEncerts(combinacioPrograma, tiradaUsuari);
	        int ple = contarPle(combinacioPrograma, tiradaUsuari);

	        if (ple == 4) {
	            System.out.println("Felicitats, has guanyat! La combinació correcta era: " +
	                    Arrays.toString(combinacioPrograma)+ ", amb"+ intents +"intents");
	            return false; // Termina la partida si hay 4 plenos
	        }

	        if (encerts > 0) {
	            System.out.println(encerts + " encertat");
	        }
	        if (ple > 0) {
	            System.out.println(ple + " ple");
	        }
	        if (encerts == 0 && ple == 0) {
	            System.out.println("0 Encerts");
	        }

	        System.out.print("Vols continuar la partida? (S/N): ");
	        String resposta = src.nextLine();
	        res = resposta.equalsIgnoreCase("S");
	        
	        if (intents == 15) {
	        	System.out.println("Ho sento has perdut...");
	        	return false;
	        }

	    } while (intents < 15); // N de intents

	    return res;
	}


	    /**
	     * Cuenta el número de elementos correctos en la posición correcta entre dos combinaciones.
	     *
	     * Este método compara cada elemento de dos arrays de enteros y cuenta cuántos de ellos
	     * son idénticos en valor y también están en la misma posición.
	     *
	     * @param combinacioPrograma La combinación generada por el programa.
	     * @param tiradaUsuari La tirada del usuario que se va a comparar.
	     * @return El número de elementos correctos en la posición correcta.
	     * @since 1.0
	     */	
		static int contarEncerts(int[] combinacioPrograma, int[] tiradaUsuari) {
	        int encerts = 0;
	        for (int i = 0; i < 4; i++) {
	            if (combinacioPrograma[i] == tiradaUsuari[i]) {
	                encerts++;
	            }
	        }
	        return encerts;
	    }
		/**
		 * Cuenta el número de plenos entre dos combinaciones.
		 *
		 * Este método compara cada elemento de dos arrays de enteros y cuenta cuántos de ellos
		 * son idénticos en valor y están en la misma posición.
		 *
		 * @param combinacioPrograma La combinación generada por el programa.
		 * @param tiradaUsuari La tirada del usuario que se va a comparar.
		 * @return El número de elementos idénticos en valor y posición entre las dos combinaciones.
		 * @since 1.0
		 */
		
	    static int contarPle(int[] combinacioPrograma, int[] tiradaUsuari) {
	        int ple = 0;
	        for (int i = 0; i < 4; i++) {
	            if (combinacioPrograma[i] == tiradaUsuari[i]) {
	                ple++;
	            }
	        }
	        return ple;
	    }
	    /**
	     * Genera y devuelve una combinación aleatoria de cuatro números entre 0 y 9.
	     *
	     * Este método inicializa un array con los números disponibles (del 0 al 9).
	     * Luego, mezcla aleatoriamente los números y selecciona los primeros cuatro para formar
	     * la combinación final.
	     *
	     * @return Un array de enteros que representa la combinación generada.
	     * @since 1.0
	     */
	    static int[] generarVec() {
	        int[] numDisponibles = new int[10];
	        for (int i = 0; i < 10; i++) {
	            numDisponibles[i] = i;
	        }

	        Random rnd = new Random();
	        for (int i = 0; i < 10; i++) {
	            int numRand = i + rnd.nextInt(10 - i);

	            int temp = numDisponibles[i];
	            numDisponibles[i] = numDisponibles[numRand];
	            numDisponibles[numRand] = temp;
	        }

	        int[] combinacion = Arrays.copyOfRange(numDisponibles, 0, 4);

	        return combinacion;
	    }
	}
