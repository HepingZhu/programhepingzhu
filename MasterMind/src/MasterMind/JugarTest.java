package MasterMind;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;



class JugarTest {

	 @Test
	    public void testContarEncerts() {
	        int[] combinacioPrograma = {1, 2, 3, 4};
	        int[] tiradaUsuari = {1, 2, 3, 4};
	        assertEquals(4, Jugar.contarEncerts(combinacioPrograma, tiradaUsuari));

	        int[] combinacioPrograma2 = {1, 2, 3, 4};
	        int[] tiradaUsuari2 = {4, 3, 2, 1};
	        assertEquals(0, Jugar.contarEncerts(combinacioPrograma2, tiradaUsuari2));

	        int[] combinacioPrograma3 = {1, 2, 3, 4};
	        int[] tiradaUsuari3 = {1, 3, 5, 4};
	        assertEquals(2, Jugar.contarEncerts(combinacioPrograma3, tiradaUsuari3));
	    }

	    @Test
	    public void testContarPle() {
	        int[] combinacioPrograma = {1, 2, 3, 4};
	        int[] tiradaUsuari = {1, 2, 3, 4};
	        assertEquals(4, Jugar.contarPle(combinacioPrograma, tiradaUsuari));

	        int[] combinacioPrograma2 = {1, 2, 3, 4};
	        int[] tiradaUsuari2 = {4, 3, 2, 1};
	        assertEquals(0, Jugar.contarPle(combinacioPrograma2, tiradaUsuari2));

	        int[] combinacioPrograma3 = {1, 2, 3, 4};
	        int[] tiradaUsuari3 = {1, 3, 5, 4};
	        assertEquals(1, Jugar.contarPle(combinacioPrograma3, tiradaUsuari3));
	    }

	    @Test
	    public void testGenerarVec() {
	        int[] combinacion = Jugar.generarVec();
	        assertEquals(4, combinacion.length);

	        for (int i = 0; i < combinacion.length; i++) {
	            assertTrue(combinacion[i] >= 0 && combinacion[i] <= 9);
	        }
	    }
}
